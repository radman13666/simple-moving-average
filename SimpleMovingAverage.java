import java.util.Deque;
import java.util.ArrayDeque;
import java.util.Scanner;
import java.math.BigInteger;
import java.math.BigDecimal;
import java.math.MathContext;

public class SimpleMovingAverage {
    // for large periods
    private BigInteger period;

    private BigInteger numberCount;

    // for arbitrarily many and arbitrarily large numbers, of arbitrary precision
    private Deque<BigDecimal> numberBuffer;

    private SimpleMovingAverage(String period) {
        setPeriod(period);
        this.numberBuffer = new ArrayDeque<>(3);
        this.numberCount = BigInteger.ZERO;
    }

    private void setPeriod(String period) {
        try {
            this.period = new BigInteger(period);
            // non-negative periods are not allowed
            if (this.period.compareTo(BigInteger.ONE) < 0) this.period = new BigInteger("3");
        } catch (Exception e) {
            // set a safe default, just in case of anything
            this.period = new BigInteger("3");
        }
    }

    public String calculate(String number) {
        String average = "x";
        // get the number, but make it zero just in case of anything
        BigDecimal n; try { n = new BigDecimal(number); } catch (Exception e) { n = BigDecimal.ZERO; }

        try {
            this.numberBuffer.addLast(n);
            this.numberCount = this.numberCount.add(BigInteger.ONE);

            if (this.numberCount.compareTo(this.period) > 0) { // if numberCount > period
                this.numberBuffer.removeFirst();
                this.numberCount = this.numberCount.subtract(BigInteger.ONE); // numberCount++
            } else if (this.numberCount.compareTo(this.period) == 0) { // if numberCount == period
                // do nothing, just skip to calculating the average
            } else {
                average = "0";
                return average;
            }

            // avg = sum / period (but round off just in case of non-terminating decimals)
            BigDecimal avg = this.sum().divide(new BigDecimal(this.period), MathContext.DECIMAL128);
            average = avg.toString();
        } catch (Exception e) {
            System.err.printf("Number #%s [%s] in the stream caused problems\n", numberCount.toString(), number);
            System.err.println(e.getMessage());
        }

        return average;
    }

    // TODO improve summation performance for large period
    private BigDecimal sum() {
        BigDecimal s = BigDecimal.ZERO;
        for (BigDecimal n : this.numberBuffer) {
            s = s.add(n);
        }
        return s;
    }

    public static void main(String[] args) {
        if (args.length != 1) return;

        SimpleMovingAverage sma = new SimpleMovingAverage(args[0]);

        Scanner inputStream = new Scanner(System.in);
        while (inputStream.hasNextLine()) {
            String num = inputStream.nextLine();
            System.out.println(sma.calculate(num));
        }
    }
}
