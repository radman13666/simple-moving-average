# Simple Moving Average

Description: [https://rosettacode.org/wiki/Averages/Simple_moving_average](https://rosettacode.org/wiki/Averages/Simple_moving_average)

My implementation takes the period as the only command-line argument and ingests the number stream from the standard input stream, one line at a time.

Since the problem did not specify how large the period might be or how large the numbers in the number stream might be, this implementation uses data types of arbitrary size and precision to calculate the simple moving average, for purposes of robustness. It also performs rounding off of the results in case of non-terminating decimals.

The program assumes that all numbers to be processed are in base 10. It has not been tested for numbers in other base systems e.g. hexadecimal.

The `numbers.txt` file contains numbers from an example in [this video](https://www.youtube.com/watch?v=WlHgUtrGalI) to test and verify that the program works correctly.

## Example run

```
$ git clone https://radman13666@bitbucket.org/radman13666/simple-moving-average.git
$ cd simple-moving-average
$ javac SimpleMovingAverage.java
$ java SimpleMovingAverage 3 < numbers.txt
0
0
278
283
287.6666666666666666666666666666667
302.3333333333333333333333333333333
305
313.6666666666666666666666666666667
313.3333333333333333333333333333333
317.3333333333333333333333333333333
314.6666666666666666666666666666667
322.3333333333333333333333333333333
320.6666666666666666666666666666667
316.6666666666666666666666666666667
312.6666666666666666666666666666667
```
